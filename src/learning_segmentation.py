"""This file is used to train a unet"""

import os
import numpy as np
from torch.utils.data import DataLoader
import torch
from torch import nn
from library.TrainingUnet import train, test
from library.Models import UNet, FocalLoss
from library.DataSetGenerator import *
from library.utils import SaveFiles as sf

#setting datasets to be used for learning
data_sets = list()
for path_to_data in ['../data/datasets/DataSet_Slide_A1702114_Magni_x10_ROI_3_Size_128_Tolerance_128_Aug_0_Bal_0.5'] :
    data_sets.append(sf.load(''.join((path_to_data,'/Dataset.p'))))

#splitting the merged datasets
data_set_train, data_set_test = MultiDataSplitter(data_sets).datasets["train"],MultiDataSplitter(data_sets).datasets["test"]

#performing data augmentation
train_data_aug = DataAugmentation(data_set_train, rotation90 = True, rotation180 = True, rotation270 = True, flip = True)

#selecting the labels on which the model is to be trained
train_data_sel = LabelSelect(train_data_aug, labels = [3])
test_data_sel = LabelSelect(data_set_test, labels = [3])

#put data in DataLoader type
inputs_train = DataLoader(train_data_sel,batch_size=100,shuffle=True)
inputs_test = DataLoader(test_data_sel,batch_size=1,shuffle=False)

#initialises the model and loss
model = UNet(in_size = 4, out_size = 2, depth = 5, dropout_conv=0.0, dropout_upconv=0.0)
criterion = FocalLoss(gamma = 2, weights = torch.tensor([2.0, 2.0]))

#training the model
_,list_loss_test = train(model, inputs_train, inputs_test, epochs = 50, path_save='../models/Segmenter_Slide_A1702114_Magni_x10_ROI_3_Size_128_Tolerance_128_Aug_0_Bal_0.5', criterion = criterion, classes = [0, 3])

#this step can be bypassed : deletes all non-optimal models for memory purposes
best_epoch_val = np.argmin(list_loss_test)
for epoch in range(epochs):
    if epoch != best_epoch_val :
        os.remove('../models/Classifier_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_True_Bal_0.5' + '/models_detect' + '/unet_epoch=50_batch=100_loss_{}_dropconv_0.0_dropupconv_0.0.pt'.format(criterion))
    else :
        path_save = '../models/Classifier_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_True_Bal_0.5' + '/models_detect' + '/unet_epoch=50_batch=100_loss_{}_dropconv_0.0_dropupconv_0.0.pt'.format(criterion)
        torch.save(torch.load(path_save).cpu(), path_save)
