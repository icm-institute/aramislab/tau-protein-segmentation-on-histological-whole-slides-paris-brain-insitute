from library.DataSetLearning import DataSetLearning
data_set = DataSetLearning(path_slides="../data/slides/",path_labels="../data/labels/",path_infos="../data/info_slides/",slides = ['A1702114'], magnification=['x20'], size = 128, stride = 128, ROI=2, tolerance = 128, data_aug = 1, balance = 0.1, random = True)
data_set.set_path("../data/datasets/DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_1_Bal_0.1")
data_set.save()