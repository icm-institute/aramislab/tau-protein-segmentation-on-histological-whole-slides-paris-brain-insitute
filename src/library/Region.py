#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 14:30:59 2020

@author: valentinabadie
"""

import numpy as np

from shapely.geometry import Polygon,Point,LinearRing
import numpy
from library.LevelSlide import *
from library.utils import SaveFiles as sf

path_slides = '../data/slides/'
path_labels = '../data/labels/'
path_infos = '../data/info_slides/'
            
class Region(LevelSlide):
    """A class to handle a rectangular region of a slide, for a specific scale
    
    Attributes :
    
        Attributes coming from LevelSlide class (class with super()) :
            path_slide : str, path to the file containing the slide (.ndpi)
            path_label : str, path to the file containing the labels (.p)
            path_info : list of str, path to the file containing the info file (.p), which is used to make slide and labels matching
            slide : openslide object
            level_dims : 2-tuple, size of the slide in pixels, at the thinner magnification level
            labels : list of 2 tuple containing the label of the associated object and its segmentation coordinates
            id : str, ID of the slide
            center : 2-tuple, coordinates of the center of the slide (in micrometers)
            step : 2-tuple, convertion rate between pixels and micrometers (in pixels/micrometers)
            limits : 2-tuple, coordinates of the limits of the slide (in micrometers)
            magnification_levels : list, levels of magnification available for the slide. If None, assumes that consecutive magnification levels have a x2 difference up to x40. Default : None.
            magnification : scaling level of the slide
            dims : dimensions of the level concerned
            _sampler_params : parameters of the sampler (size, stride, offset, borders, ROI, ROIp)
            sampler : an iterable wich samples regions from the level, according to the parameters settled in _sampler_params
            
        array_region : array containing the value of the pixels of the region
        coordinates : 4-tuple of ints  containing the x1,y1,x2,y2 coordinates of the region
        in_labels : list, labels intersecting the region
        shape : 3-tuple, shape of the region array
        size : 2-tuple, size on the region
        colors : list, list of colors used to display labels properly.
        read_labels : bool, if True reads the intersecting labels. Default : True.
        read_mask : bool, if True makes a mask out of intersecting labels. Default : True.
        
    Methods :
    
        Built-in :
            __init__ : initialises the class.
            __str__ : displkays a visual aspect of the region
            
        Special :
            reshape_meter : takes input coordinates in meters, and places it inside the region if outside.
            save : saves the regions with its labels and mask
        
        Backend :
            _read_region : retrieves and read the wanted rectangular region.
            _read_labels : retrieves slide labels intersecting the region.
            _read_mask : computes the mask of the region out of its labels.
            
        
        
    To read a region, you must input the path, the level of the slide and the 4 coordinates
    delimiting the region x1,y1,x2,y2. You specify to what kind of coordinates that corresponds,
    via the parameter type_coord which can be set either to 'meter' or 'pixel'. Default to 'meter'.
    
    """
    
    def __init__(self, coordinates, path_slide, path_label = None, path_info = None, magnification = -1, type_coord='meter',read_labels = True, read_mask = True):
        
        """Initialises the class
        
        Inputs :
            path_slide : str, path to the file containing the slide (.ndpi)
            path_label : str, path to the file containing the labels (.p)
            path_info : list of str, path to the file containing the info file (.p), which is used to make slide and labels matching
            magnification : int or str, if int returns the magnification-th magnification level, starting from the greatest down to the smaller, if str return the corresponding magnification level. Default : -1.
            coordinates : 4-tuple of ints  containing the x1,y1,x2,y2 coordinates of the region
            type_coord : str. If 'meter', the coordinates passed as input will be considered as real life meter coordinates, if 'pixel' it will be considered as pixel values on the magnification level. Default : 'meter'.
            read_labels : bool, if True reads the intersecting labels. Default : True.
            read_mask : bool, if True makes a mask out of intersecting labels. Default : True.
            
        """
        
        
        super().__init__(path_slide,path_label = path_label, path_info = path_info, magnification = magnification)
        
        coordinates = sorted([coordinates[0],coordinates[2]])+sorted([coordinates[1],coordinates[3]])
        self.coordinates = [coordinates[0],coordinates[2], coordinates[1],coordinates[3]]
        
        self._read_region(type_coord)

        
        self.colors = ['blue','green','red','white']
        
        self.shape = self.array_region.shape
        self.size = (self.shape[1],self.shape[0])
        
        if read_labels and (path_label is not None) and  (path_info is not None):
            self._read_labels()
            if read_mask :
                self._read_mask()
    
    def _read_region(self,type_coord):
        """reads the region specified as input
       
        Inputs :
            type_coord : str. If 'meter', the coordinates passed as input will be considered as real life meter coordinates, if 'pixel' it will be considered as pixel values on the magnification level. Default : 'meter'.
        
        """
        
        if type_coord == 'meter':
            xp1,yp1,xp2,yp2 = self.meter_to_pixel(self.coordinates)

            xp1,yp1,xp2,yp2 = self.reshape_pixel(xp1,yp1,xp2,yp2)

            #permutation of coordinates
            xp1,xp2 = sorted([xp1,xp2])
            yp1,yp2 = sorted([yp1,yp2])
            
            #retrieves th corresponding poisitions in meters
            self.positions = np.meshgrid(np.linspace(self.coordinates[0],self.coordinates[2],xp2-xp1),np.linspace(self.coordinates[1],self.coordinates[3],yp2-yp1))
            
            top_left = (xp1*2**self.magnification,yp1*2**self.magnification)
            size = (xp2-xp1,yp2-yp1)
            
            self.array_region = np.array(self.slide.read_region(top_left,self.magnification,size))
        
        if type_coord == 'pixel' :
            xp1,yp1,xp2,yp2 = self.coordinates
            
            coordinates = self.pixel_to_meter(self.coordinates)
            coordinates = sorted([coordinates[0],coordinates[2]])+sorted([coordinates[1],coordinates[3]])
            self.coordinates = [coordinates[0],coordinates[2], coordinates[1],coordinates[3]]
            
            xp1,yp1,xp2,yp2 = self.reshape_pixel(xp1,yp1,xp2,yp2)
            
            #permutation of coordinates
            xp1,xp2 = sorted([xp1,xp2])
            yp1,yp2 = sorted([yp1,yp2])
            
            #retrieves th corresponding poisitions in meters
            self.positions = np.meshgrid(np.linspace(self.coordinates[0],self.coordinates[2],xp2-xp1),np.linspace(self.coordinates[1],self.coordinates[3],yp2-yp1))
            
            top_left = (xp1*2**self.magnification,yp1*2**self.magnification)
            size = (xp2-xp1,yp2-yp1)
            
            self.array_region = np.array(self.slide.read_region(top_left,self.magnification,size))
    
    def _read_labels(self):
        """gets the labels that intersect the region, and modify them to display properly"""
        
        self.in_labels = list()
        
        #creates a polygon object corresponding to the region 
        x1,y1,x2,y2 = self.coordinates
        polygon_region = Polygon(((x1,y1),(x2,y1),(x2,y2),(x1,y2)))

        #check, for every labelled object, if it intersects the region
        for object_type,coordinates in self.labels :
            #creates a polygon object
            polygon_label = Polygon([(point[0],point[1]) for point in coordinates])
            
            #checking intersection
            if polygon_label.intersects(polygon_region) :
                try :
                    in_coordinates = np.array(list((polygon_label.intersection(polygon_region)).exterior.coords))
                    self.in_labels.append((object_type,in_coordinates))
                except :
                    pass
        
    
    def reshape_meter(self, *args):
        """Sets all pixel coordinates outside of the image to be inside instead
        
        Arguments can be : 
            1 - type of coordinate,x1,x2,...: type_of_coordinate being a string ('x' for x axis, 'y' for y axis)
            2 - (x1,y1,x2,y2,...) : a tuple of paired coordinates
            3 - (x1,y1),(x2,y2),... : a list of 2-tuple pairs of coordinates
            4 - array([[x1,y1],[x2,y2],...]) : a n-by-2 array of coordinates
        """
        
        x_m,x_M = sorted([self.coordinates[0],self.coordinates[2]])
        y_m,y_M = sorted([self.coordinates[1],self.coordinates[3]])
        
        def rsm_en_arr(array):
            """this function is the restriction of pixel_to_meter to the 4th type of input data (array)"""

            bool_array_x_min = (array[:,0] >= x_m).astype(float)
            bool_array_y_min = (array[:,1] >= y_m).astype(float)
            bool_array_x_max = (array[:,0] <= x_M).astype(float)
            bool_array_y_max = (array[:,1] <= y_M).astype(float)
            
            array[:,0] = bool_array_x_min * bool_array_x_max * array[:,0] + ((1 - bool_array_x_min) * x_m) + ((1 - bool_array_x_max) * x_M)
            array[:,1] = bool_array_y_min * bool_array_y_max * array[:,1] + ((1 - bool_array_y_min) * y_m) + ((1 - bool_array_y_max) * y_M)

            return array
        
        # case 1
        if type(args[0]) is str :
            
            #since the first arg is to tell the type, the rest is only taken into account
            to_convert = np.array(args[1:]).reshape(-1,1)
            converted = rsm_en_arr(np.concatenate((to_convert, to_convert), 1))
            
            #the output depends on the type of input
            if args[0] == 'x':
                return tuple(converted[:,0])
            if args[0] == 'y':
                return tuple(converted[:,1])
         
        #case 2
        if (type(args[0]) is float) or (type(args[0]) is int):
            
            #sets the 2n sized input to a n*2 array
            args = np.array(args)
            x_index = [x for x in range(0, len(args), 2)]
            y_index = [y for y in range(1, len(args), 2)]
            array_args = np.concatenate([args[x_index].reshape(-1,1), args[y_index].reshape(-1,1)], 1)
            
            #converts the values
            converted_array =  rsm_en_arr(array_args)

            #reshapes the result to have an input-like output
            args_out = np.zeros(len(args)).astype(int)
            args_out[x_index] = converted_array[:,0].reshape(1, -1)
            args_out[y_index] = converted_array[:,1].reshape(1, -1)
            
            return tuple(args_out)
        
        #case 3
        if (type(args[0]) is tuple) or (type(args[0]) is list) :
            if (type(args[0][0]) is tuple) or (type(args[0][0]) is list) :
                return tuple([tuple(line) for line in rsm_en_arr(np.array(args[0]))])
            else :
                if len(args[0])==2:
                    return tuple([tuple(line) for line in rsm_en_arr(np.array(args))])
                else :
                    args = np.array(args[0])
                    x_index = [x for x in range(0, len(args), 2)]
                    y_index = [y for y in range(1, len(args), 2)]
                    array_args = np.concatenate([args[x_index].reshape(-1,1), args[y_index].reshape(-1,1)], 1)
                    
                    #converts the values
                    converted_array =  rsm_en_arr(array_args)
        
                    #reshapes the result to have an input-like output
                    args_out = np.zeros(len(args)).astype(int)
                    args_out[x_index] = converted_array[:,0].reshape(1, -1)
                    args_out[y_index] = converted_array[:,1].reshape(1, -1)
                    
                    return tuple(args_out)
        #case 4
        if type(args[0]) is numpy.ndarray :
            return rsm_en_arr(args[0])
    
    def _read_mask(self,priority=[1,0,2,3]):
        
        """ Computes the mask of the region. It outputs an array of the same size as the region, containing
        intergers which indicate whether a pixel belongs or not to a given object class (0 for nor class,
        and 1, 2, 3... according to the classes passed as argument)
        
        Inputs :
            - priority : list or tuple of integers. if several classes overlap, only one can be represented
            in the final result. Hence, a priority order has to be given. The priority argument is a list
            containing the classes in the inverse order of priority : [3,0,1,2] indicates 2 class to has prio-
            rity on class 1, which has priority on class 0 which has priority on class 3. If priority is passed
            as 'default', the order is [0,1,2,3,...]
        
        """
        
        mask = np.zeros(np.flip(self.size))
        
        if self.in_labels == []:
            self.mask = mask
            return
        
        if priority == 'default':
            priority = list(sorted(np.unique(list(list(zip(*self.in_labels))[0]))))
        
        #for each label in priority order, retrieves pixels belonging to the corresponding class
        for label in priority :
            coordinates_label = list(zip(*list(filter(lambda x:x[0] == label,self.in_labels))))
            #print(coordinates_label)
            if coordinates_label:
                for coordinates in list(coordinates_label[1]) :
                    
                    #creates a Polygon from the coordinates detouring the object
                    polygon_label = Polygon([(point[0],point[1]) for point in coordinates])
                    edges_label = LinearRing([(point[0],point[1]) for point in coordinates])
                    
                    #creating a frame of pixel positions including the mask to retrieve
                    X,Y = self.positions
                    mask_contour = (X>=np.min(coordinates[:,0]))* (X<=np.max(coordinates[:,0]))*(Y>=np.min(coordinates[:,1]))* (Y<=np.max(coordinates[:,1]))
                    X_in,Y_in = X[mask_contour],Y[mask_contour]
                    pixels = np.vstack((X_in.flatten(),Y_in.flatten())).T
                    
                    #computing the mask of the concerned object
                    mask_contour[mask_contour] = np.array([polygon_label.contains(Point(pixel[0],pixel[1])) or edges_label.contains(Point(pixel[0],pixel[1])) for pixel in pixels])
                    mask[mask_contour] = label*(mask_contour[mask_contour].astype(float))
                
        self.mask = np.flip(mask,0)
         
    def __str__(self):
        
        """ Displays the region, with its labels, and the corresponding mask"""
        
        from matplotlib.pyplot import subplots
        fig,ax = subplots(1,int('mask' in self.__dict__)+1,figsize=(10,10*(int('mask' in self.__dict__)+1)))
        
        if 'mask' in self.__dict__ :
            ax[0].imshow(self.array_region, cmap='gray', extent = [self.coordinates[0],self.coordinates[2],self.coordinates[1],self.coordinates[3]])
            
            for val in self.in_labels :
                ax[0].plot(val[1][:,0], val[1][:,1],color = self.colors[val[0]])
                
            ax[1].imshow(self.mask, cmap='gray', extent = [self.coordinates[0],self.coordinates[2],self.coordinates[1],self.coordinates[3]])
                
        else :
            ax.imshow(self.array_region, cmap='gray', extent = [self.coordinates[0],self.coordinates[2],self.coordinates[1],self.coordinates[3]])
            if 'in_labels' in self.__dict__ :
                for val in self.in_labels :
                    ax.plot(val[1][:,0], val[1][:,1],color = self.colors[val[0]])
        
        return ''
    
    def save(self,path=None):
        """saves the region object in a file
        
        Inputs :
            path : the path folder where to save the region.
        
        """
        
        if path is None :
            path = self.path_regions
        if 'in_labels' not in self.__dict__ :
            self._read_labels()
        if 'mask' not in self.__dict__ :
            self._read_mask()
            
        infos_to_save = {'image' : self.array_region, 'mask' : self.mask, 'labels' : self.in_labels, 'magnification': self.magnification, 'coordinates': self.coordinates,'id': self.id}
        
        name = 'region_{}_{}_{}_{}_{}_{}'+'.p'
        name = name.format(self.id,self.magnification,self.coordinates[0],self.coordinates[1],self.coordinates[2],self.coordinates[3])
        
        if path[-1] == '/':
            sf.save(path + name, infos_to_save)
        else :
            sf.save(path +'/'+ name, infos_to_save)