import os
import numpy as np
from torch.utils.data import DataLoader
import torch
from torch import nn
from library.TrainingClassifier import train_detect
from library.Models import DetectionCNN
from library.DataSetLearning import *
from library.utils import SaveFiles as sf
from library.Losses import FocalLoss
criterion = FocalLoss(gamma = 5, weights = torch.tensor([2.0, 2.0]))
data_sets = list()
for path_to_data in ['../data/datasets/DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_1_Bal_0.5'] :
    data_sets.append(sf.load(''.join((path_to_data,'/Dataset.p'))))
    data_sets[-1].set_path(path_to_data)
    data_sets[-1]._show_coord = False
    data_sets[-1].load = True #specifies that the dataset has been loaded
    data_sets[-1].aug = False
data_set_train, data_set_test = MultiDataSplitter(data_sets).datasets["train"],MultiDataSplitter(data_sets).datasets["test"]
data_set_train = Normalization(data_set_train, np.array(data_set_train[0]['Images']))
data_set_test = Normalization(data_set_test, np.array(data_set_train[0]['Images']))
train_data_aug = DataAugmentation(data_set_train, rotation90 = True, rotation180 = True, rotation270 = True, flip = True)
train_data_detec = DetectionOnly(train_data_aug, label = [2])
test_data_detec = DetectionOnly(data_set_test, label = [2])
inputs_train = DataLoader(train_data_detec,batch_size=100,shuffle=True)
inputs_test = DataLoader(test_data_detec,batch_size=1,shuffle=False)
print('Model : Magnification = x20, Size = 128, ROI = [2], Tolerance = 128, Depth = 5')
model = DetectionCNN(in_size = 4, out_size = 4, in_shape = 128, dropout_conv=0.0, dropout_fully=0.0)
_,_,list_loss_test, list_f1 = train_detect(model, inputs_train, inputs_test, epochs = 50, path_save='../models/Classifier_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_True_Bal_0.5', criterion = criterion)
print('Model : Magnification = x20, Size = 128, ROI = [2], Depth = 5, Tolerance = 128')
best_epoch_val = np.argmin(list_loss_test)
best_epoch_f1 = np.argmax(list_f1)
for epoch in range(epochs):
    if epoch != best_epoch_val and epoch != best_epoch_f1:
        os.remove('../models/Classifier_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_True_Bal_0.5' + '/models_detect' + '/cnn_epoch=50_batch=100_loss_{}_dropconv_0.0_dropfully_0.0.pt'.format(criterion))
    else :
        path_save = '../models/Classifier_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_True_Bal_0.5' + '/models_detect' + '/cnn_epoch=50_batch=100_loss_{}_dropconv_0.0_dropfully_0.0.pt'.format(criterion)
        torch.save(torch.load(path_save).cpu(), path_save)