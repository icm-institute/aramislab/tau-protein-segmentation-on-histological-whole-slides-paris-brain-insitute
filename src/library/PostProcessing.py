#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 15:27:24 2020

@author: valentinabadie
"""

import numpy as np
from scipy.ndimage.filters import gaussian_filter

""" This module is designed to perform post-processing tasks, for now on tangles. 
The principle is to detect gaussian spots on 2D probability maps given by the neural network. Then, it should be applied to discriminate whether it is a correctly detected one or not.

"""
    
class GaussianSpot():
    
    def __init__(self, *args):
        if len(args) == 1:
            self.t = args[0][0]
            self.mean = np.array(args[0][1:3]).reshape(2,1)
            self.lambdas = np.array(args[0][3:5]).reshape(2,1)
            self.theta = args[0][5]
            
        else :
            self.t = args[0]
            self.mean = np.array(args[1]).reshape(2,1)
            self.lambdas = np.array(args[2]).reshape(2,1)
            self.theta = args[3]
            
    def __call__(self, x):
        return self.t*np.exp(-np.sum((x-self.mean)*np.linalg.multi_dot( (self._rotation_matrix(-self.theta),np.diag(1/self.lambdas.flatten()),self._rotation_matrix(self.theta),(x-self.mean))), 0) )

    def _rotation_matrix(self, theta):
        return np.array([[np.cos(theta), - np.sin(theta)],[np.sin(theta), np.cos(theta)]])

    def gradient(self, x):
        pass

    def gradient_params(self, x):
        grad_t = 1/self.t
        
        grad_mean = 2*np.sum(np.linalg.multi_dot((self._rotation_matrix(-self.theta),np.diag(1/self.lambdas.flatten()),self._rotation_matrix(self.theta),(x-self.mean))), 1).reshape(2,1)
        
        grad_lambd = 1/self.lambdas**2 * np.sum((x-self.mean)*np.linalg.multi_dot((self._rotation_matrix(-self.theta),np.diag([1,0]),self._rotation_matrix(self.theta),(x-self.mean))), 1).reshape(2,1)
        
        grad_theta = 2*np.sum((x-self.mean)*np.linalg.multi_dot((self._rotation_matrix(-self.theta + np.pi/2),np.diag([1,0]),self._rotation_matrix(self.theta),(x-self.mean))))
        
        vector_grad = np.vstack((grad_t, grad_mean, grad_lambd, grad_theta))

        return vector_grad*self.__call__(x)
    
    def __add__(self, other):
        
        self.__call__ = lambda x: self.__call__(x) + other.__call__(x)
        self.gradient_params = (lambda x: np.vstack((self.gradient_params(x),other.gradient_params(x))))
        
        return self
    
class PowerError():
    
    def __init__(self, power):
        self.power = power
    
    def __call__(self, x):
        return x**self.power
    
    def gradient(self,x):
        return self.power*x**(self.power-1)

class LossGaussian():
    
    def __init__(self, data, fun, model):
        self.data = data
        self.fun = fun
        self.model = model
        
    def __call__(self, *args):
        return np.sum(self.fun(self.data['y'] - self.model(*args)(self.data['x'])))
        
    def gradient(self, *args):
        return -np.sum(self.model(*args).gradient_params(self.data['x'])*self.fun.gradient(self.data['y'] - self.model(*args)(self.data['x'])), 1)

class GradientDescent():
    
    def __init__(self, fun, precision = 0.01, control = 0.4, decrease = 0.9):
        self.precision = precision
        self.control = control
        self.decrease = decrease
        self.fun = fun
        
    def _step_classic(self, parameters, step = 0.1):
        
        grad = self.fun.gradient(parameters)
        return parameters - step * grad/np.linalg.norm(grad)
    
    def _step_armijo(self, parameters, step = 1):
        
        grad = self.fun.gradient(parameters)
        descent_dir = - grad / np.linalg.norm(grad)
        m = np.sum(grad * descent_dir)
        t = - self.control * m
        
        while self.fun(parameters) - self.fun(parameters + step * descent_dir) < step * t :
            print(self.fun(parameters) - self.fun(parameters + step * descent_dir), step * t)
            step = step * self.decrease
            
        return parameters + step * descent_dir
        
    def gradient_descent(self, *parameters, step = 'armijo', max_steps = 100):
        
        self.iterations = 0
        self.gradients = [self.fun.gradient(parameters)]
        
        while (np.linalg.norm(self.gradients[-1]) > self.precision)  and (self.iterations < max_steps):
            parameters = getattr(self, ''.join(('_step_', step)))(parameters)
            self.iterations += 1
            self.gradients += [self.fun.gradient(parameters)]
            
        return parameters
    
class RetrieveGaussians():
    
    def __init__(self, data, number):
        self.data = data
        self.number = number

        t_init = np.max(data['y'])
        mean_init = data['x'][np.argmax(data['y'])]
        lambda_init = np.array([[1],[1]])
        theta_init = 0
        
        self.init_params = np.vstack((mean_init, lambda_init, theta_init, t_init))
        
    def __call__(self, precision = 0.01, control = 0.1, decrease = 0.9, fun = None, init = None):
        if fun is None :
            fun = LossGaussian(self.data, PowerError(2), GaussianSpot)
        else :
            pass
        
        if init is not None:
            parameters = init
        else :
            parameters = self.init_params
        
        return GradientDescent(precision, control, decrease, fun)(init)

class DetectLocalMaxima():
    """
    A class to perform local maxima of probability for having a tangle/plaque on an image.
    
    Attributes :
    
        - dataset : iterable, the input dataset
        - outputs : iterable, the outputs corresponding to every dataset entry
        - sigma : float, smoothing parameter to reduce noise before local max detection
    
    """
    
    def __init__(self, dataset, outputs, sigma = 1):
        
        self.dataset = dataset
        self.outputs = outputs
        self.dataset._show_coord = True
        
        self.sigma = sigma
        
        self._collect_regions()
    
    def _collect_regions(self):
        
        #self.regions = np.array(sorted(enumerate(self.dataset.regions), key = lambda x: (float(x[1].split('_')[3]),float(x[1].split('_')[4]))))
        #self.indices = self.regions[:,0].astype(int)
        #self.regions = self.regions[:,1]
        #self.outputs = np.array(self.outputs)[self.indices]
        #self.dataset.regions = self.regions
        
        #region_dict = dict()
        #np.linspace()
        
        
        self.regions = self.dataset.regions
        
        region_positions = np.array([(float(x.split('_')[3]), float(x.split('_')[4])) for x in self.regions])
        
        x_pos = np.sort(np.unique([float(x.split('_')[3]) for x in self.regions]))
        y_pos = np.sort(np.unique([float(x.split('_')[4]) for x in self.regions]))
        
        self.region_arr = np.zeros((len(x_pos), len(y_pos)))
        self.coord_arr = np.zeros((len(x_pos), len(y_pos)))
        
        from tqdm import tqdm
        for i, x in tqdm(enumerate(x_pos)):
            for j, y in enumerate(y_pos):
                try :
                    region_ind = np.argwhere((region_positions[:,0] == x) *(region_positions[:,1] == y))
                    
                    output = self.outputs[region_ind.flatten()[0]]
                    
                    self.region_arr[i,j] = output
                    self.coord_arr[i,j] = region_ind.flatten()[0]

                except :
                    pass
        
    def detection(self):
        
        #blurred = gaussian_filter(self.region_arr, sigma=self.sigma)
        blurred = self.region_arr
        blurred_th = np.zeros(blurred.shape)
        blurred_pad = np.pad(blurred, 1)
        for i in range(blurred.shape[0]):
            for j in range(blurred.shape[1]):
                boo1 = (blurred_pad[i+1,j+1] > blurred_pad[i,j+1])
                boo2 = (blurred_pad[i+1,j+1] > blurred_pad[i+1,j])
                boo3 = (blurred_pad[i+1,j+1] > blurred_pad[i+2,j+1])
                boo4 = (blurred_pad[i+1,j+1] > blurred_pad[i+1,j+2])
                
                blurred_th[i,j] = int(boo1*boo2*boo3*boo4)
                
        #x_s = np.vstack((np.ones((1,blurred.shape[1])).astype(bool), blurred[1:,:] <= blurred[:-1,:]))
        #x_i = np.vstack((blurred[1:,:] >= blurred[:-1,:], np.ones((1,blurred.shape[1])).astype(bool)))
        #y_s = np.hstack((np.ones((blurred.shape[0],1)).astype(bool), blurred[:,1:] <= blurred[:,:-1]))
        #y_i = np.hstack((blurred[:,1:] >= blurred[:,:-1], np.ones((blurred.shape[0],1)).astype(bool)))
        
        #blurred[np.invert(x_s*x_i*y_s*y_i)] = 0
        
        #blurred[blurred != 0] = 1
        
        return blurred, blurred_th
    