#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 14:30:59 2020

@author: valentinabadie
"""

import numpy as np
import numpy
import matplotlib.pyplot as plt
from openslide import OpenSlide
import os
import cv2
from skimage import io, color

from library.DataSetSlides import DataSetSlides
from library.utils import SaveFiles as sf
from library.utils import ColorTransforms as ct
from library.octrees.octrees import octree_from_list

class KhanNormalization():
    
    def __init__(self, dataset_train, dataset_test, labels):
        
        self.dataset_train = dataset_train
        self.dataset_test = dataset_test
        self.labels = labels
        
    def _make_octree(self):
        pixel_list_train = np.hstack([data[:,:,:3].reshape(-1, 3) for data in self.dataset_train])
        self.octree_data = octree_from_list(((0,255),(0,255),(0,255)), pixel_list_train)

class Normalize(object):
    
    """This class defines a general framework to perform different types of normalization on the dataset
    
    Attributes :
        - paths : several paths pointing at required data
        - reference_region : the region which serves as a reference for performing the normalization
    
    """

    def __init__(self, reference_image, reference_mask=None, path=None):
        
        self.reference_image = reference_image 
        if reference_mask is None:
            self.set_mask()
        else:
            self.reference_mask = reference_mask
            
        self.path = path
        
    def set_path(self, path):
        self.path = path    
        
    def set_mask(self, blue = (0.5,0.6), brown_low = (0.,0.15), brown_up = (0.8,1.)):
        self.reference_mask = SetColorClasses(self.reference_image)(blue = (0.5,0.6), brown_low = (0.,0.15), brown_up = (0.8,1.))[1]
        
        
class NormalizeReinhard(Normalize):
    """A class to perform Reinhard and al. normalization of a region"""
    
    def __init__(self,reference_image, reference_mask=None, path=None ):
        
        super().__init__(reference_image, reference_mask=reference_mask, path=path)
        
        self._ref_statistics()
        
    def _ref_statistics(self): 
        """Computes the useful statistics of the reference region"""
        
        self.avg_ref = dict()
        self.sigma_ref = dict()
        
        ref_region_lab = ct.rgb2lab(self.reference_image)
        
        for label in np.unique(self.reference_mask):
            self.avg_ref[label] = np.mean(ref_region_lab[self.reference_mask == label][:,:3],0)
            self.sigma_ref[label] = np.std(ref_region_lab[self.reference_mask == label][:,:3],0)

    def normalise(self,image, blue = (0.5,0.6), brown_low = (0.,0.15), brown_up = (0.8,1.)):
        """normalises the selected region for it to have the same standard deviation and average 
        as the reference region for every label"""
        
        mask = SetColorClasses(image)(blue = blue, brown_low = brown_low, brown_up = brown_up)[1]
        
        image_lab = ct.rgb2lab(image)
        
        image_lab_norm = np.zeros(image_lab.shape)

        for label in np.unique(self.reference_mask):
            avg = np.mean(image_lab[mask == label][:,:3],0)
            sigma = np.std(image_lab[mask == label][:,:3],0)
            image_lab[mask == label] = np.concatenate(((image_lab[mask == label][:,:3] - avg)/sigma*self.sigma_ref[label] + self.avg_ref[label],image_lab[mask == label][:,3].reshape(-1,1)),1)
        for label in np.unique(mask):
            avg = np.mean(image_lab_norm[mask == label],0)
            sigma = np.std(image_lab_norm[mask == label],0)
        
        return ct.lab2rgb(image_lab),mask
    
class SetColorClasses():
    """Class to handle the classification of colors on the images"""
    
    def __init__(self, image):
        self.image = image[:,:,:3]

    def _make_labels(self, blue = (0.5,0.6), brown_low = (0.,0.15), brown_up = (0.8,1.)):
        hue_image = np.array(cv2.cvtColor(self.image.astype(numpy.uint8), cv2.COLOR_RGB2HSV))[:,:,0].astype(float)/180
        
        self.mask = np.zeros(self.image.shape[:2])

        inter_brown_low = (brown_low[0] < hue_image)*(brown_low[1] > hue_image)
        self.mask[inter_brown_low] =  (inter_brown_low[inter_brown_low]).astype(float)

        inter_brown_up = (brown_up[0] < hue_image)*(brown_up[1] > hue_image)
        self.mask[inter_brown_up] =  (inter_brown_up[inter_brown_up]).astype(float)
        
        inter_blue = (blue[0] < hue_image)*(blue[1] > hue_image)
        self.mask[inter_blue] =  2*((inter_blue[inter_blue]).astype(float))
        
        self.mask = self.mask.astype(int)
        
    def __call__(self, blue = (0.5,0.6), brown_low = (0.,0.15), brown_up = (0.8,1.)):
        self._make_labels(blue = blue, brown_low = brown_low, brown_up = brown_up)
        
        return self.image, self.mask
