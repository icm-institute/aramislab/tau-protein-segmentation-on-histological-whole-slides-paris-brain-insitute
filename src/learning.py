"This file is used to perform training of classifier models"""


import os
import numpy as np
from torch.utils.data import DataLoader
import torch
from torch import nn
from library.Training import train_classifier
from library.Models import DetectionCNN, FocalLoss, CrossEntropyLoss
from library.DataSetGenerator import *
from library.utils import SaveFiles as sf

in_size = 4 #set to 4 if there is no normalization, and to 3 otherwise
out_size = 2 #set to as much labels to be retrieved (for example, 2 for tangle and other, 3 for tangle, plaque and other, 4 for tangle, plaque, gray matter and other)
depth = 5

#setting loss to be used to train the model
criterion = FocalLoss(gamma = 2, weights = torch.tensor([1.1111111111111112, 10.0]))

#loading datasets on which to train the model
data_sets = list()
for path_to_data in ['../data/datasets/DataSet_Slide_A1702114_Magni_x10_ROI_3_Size_128_Tolerance_128_Aug_1_Bal_0.5','../data/datasets/DataSet_Slide_A1702076_Magni_x10_ROI_3_Size_128_Tolerance_128_Aug_1_Bal_0.5'] : #to use multiple folders
    data_sets.append(sf.load(''.join((path_to_data,'/Dataset.p'))))

#merging and splitting the datasets
data_set_train, data_set_test = MultiDataSplitter(data_sets).datasets["train"],MultiDataSplitter(data_sets).datasets["test"]

#performing data augmentation
train_data_aug = DataAugmentation(data_set_train, rotation90 = False, rotation180 = False, rotation270 = False, flip = False)

#switching to detection task
train_data_detec = DetectionOnly(train_data_aug, label = 3)
test_data_detec = DetectionOnly(data_set_test, label = 3)

#putting data in a proper object type
inputs_train = DataLoader(train_data_detec,batch_size=10,shuffle=True)
inputs_test = DataLoader(test_data_detec,batch_size=1,shuffle=False)

print('Model : Magnification = x20, Size = 128, ROI = 3, Tolerance = 128, Depth = {}'.format(depth))
      
#selecting model to be used
model = DetectionCNN(in_size = in_size, out_size = out_size, depth = depth, dropout_conv=0.0, dropout_fully=0.0)

#performing training
_,_,list_loss_test, list_f1 = train_classifier(model, inputs_train, inputs_test, epochs = 50,   path_save='../models/Normalized_Slide_A1702114_Magni_x10_ROI_3_Size_128_Tolerance_128_Bal_0.5_Aug_1', criterion = criterion)

#### THIS STEP CAN BE BYPASSED, IT IS ONLY FOR MEMORY ECONOMY PURPOSES 

#retrieving best epochs performed
#best_epoch_val = np.argmin(list_loss_test)
#best_epoch_f1 = np.argmax(list_f1)

#deleting other epochs
#for epoch in range(epochs):
#    if epoch != best_epoch_val and epoch != best_epoch_f1:
#        os.remove('../models/Classifier_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Bal_0.1' + '/models_detect' + '/cnn_epoch=50_batch=10_loss_{}_dropconv_0.0_dropfully_0.0.pt'.format(criterion))
#    else :
#        path_save = '../models/Classifier_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Bal_0.1' + '/models_detect' + '/cnn_epoch=50_batch=10_loss_{}_dropconv_0.0_dropfully_0.0.pt'.format(criterion)
#        torch.save(torch.load(path_save).cpu(), path_save)
